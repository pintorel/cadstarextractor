import sys
from CADSTARExtractor import CADSTARExtractor

# python test.py <path to the source .scm file> <path to the target CSV file>
def main():
    extractor = CADSTARExtractor()
    extractor.csv_bill_of_materials(sys.argv[1], sys.argv[2])

if __name__ == "__main__":
    main()