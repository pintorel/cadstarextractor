from setuptools import setup
setup(name='CADSTARExtractor',
version='0.1',
description='Extracts information from CADSTAR files',
url='#',
author='Raul Torres',
author_email='raultorrescarvajal@gmail.com',
license='MIT',
packages=['CADSTARExtractor'],
zip_safe=False)