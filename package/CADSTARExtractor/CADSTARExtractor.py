import win32com.client
import os

class CADSTARExtractor:
    
    def csv_bill_of_materials(self, design_file_name, csv_file_name):
        # init the app and open the file
        os.system("\"C:\\Program Files (x86)\\Zuken\\CADSTAR Express 2018.0\\Programs\\express.exe\"") #update here the location of your own installation
        app = win32com.client.Dispatch("CADSTAR.Application")
        app.Visible(False)
        app.OpenAsReadOnly2(design_file_name)

        # get the info
        design = app.GetCurrentDesign
        components = design.GetComponents
        nr_components = components.GetCount

        # build a list of strings with the extracted info
        csv_lines = ["Name,Part Name,Part Number,Part Description"]
        i = 0
        while  i < nr_components:
            item = components.GetItem(i)
            part = item.GetPart
            csv_line = item.Name + "," + item.GetPartName + "," + part.GetNumber + "," + part.GetDescription # info extracted
            csv_lines.append(csv_line)
            i = i + 1

        # write the extracted info to a CSV file
        with open(csv_file_name, 'w') as csv_file:
            for csv_line in csv_lines:
                csv_file.write("%s\n" % csv_line)

        # close everything
        design.Close
        app.Exit
        os.system("taskkill /im desedit.exe /f")
