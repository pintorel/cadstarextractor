# CADSTAR Extractor

Extracts the bill of materials from a CADSTAR design using Python.

## Assumptions

We assume your CADSTAR installation is located under:

	"C:\Program Files (x86)\Zuken\CADSTAR Express 2018.0\Programs\"
	
If this is not correct, change the code accordingly in the following file:

	THIS_FOLDER\package\CADSTARExtractor\CADSTARExtractor.py


## Instructions

* Install module pywin32:
```bash
	pip install pywin32
```
* The "package" folder contains the module files. 
* To install the module, do:
```bash
	cd package
	pip install .
```	
* To unistall the package, do:
```bash
	pip uninstall CADSTARExtractor
```	
* The "test" folder contains an example on how to use the extractor.
* To run the test program, do:
```bash
	python test.py <path to the source .scm file> <path to the target CSV file>
```	
	

	
	